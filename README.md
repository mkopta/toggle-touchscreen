# Gnome Shell extension to toggle touchscreen
![](http://i.imgur.com/XjceicU.png)

## How to install

Go to https://extensions.gnome.org/extension/991/toggle-touchscreen/ and install from there.

Make sure you have `xinput` installed (`sudo apt-get install xinput`).

### How to install manually

Download latest release (https://github.com/mkopta/toggle-touchscreen/releases/download/v1.4/toggle-touchscreen-v1.4.zip) and unpack it to this directory `~/.local/share/gnome-shell/extensions/toggle-touchscreen@martin.kopta.eu`. Restart the gnome shell by ALT+F2, type `r` and enter.

Commands to install
```
rm -rf ~/.local/share/gnome-shell/extensions/toggle-touchscreen@martin.kopta.eu
mkdir -p ~/.local/share/gnome-shell/extensions/toggle-touchscreen@martin.kopta.eu
cd ~/.local/share/gnome-shell/extensions/toggle-touchscreen@martin.kopta.eu
curl -LO https://github.com/mkopta/toggle-touchscreen/releases/download/v1.4/toggle-touchscreen-v1.4.zip
unzip toggle-touchscreen-v1.4.zip
rm toggle-touchscreen-v1.4.zip
```

Now ALT+F2, type `r` and enter.

Make sure you have `xinput` installed (`sudo apt-get install xinput`).

### In case of trouble

Use gnome tweak tool to see whether the extension is really there and enabled.

Use looking glass to see more details (ALT+F2 lg)


(based on https://github.com/mkopta/toggle-touchpad)
